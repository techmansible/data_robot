datarobot_install:

1. Download the binary from artifactory
2. Untar the binary to datarobot directory
3. Copy the config file from template 
4. Validate the configuration
5. Install Dependencies 
6. Start the Docker registry
7. Execute Pre-flight Checks
8. Install the datarobot app
9. Datarobot services restart
10. Execute command to generate the initial admin account for DataRobot
11. Delete the binary from tmp directory
